#!/bin/bash -xe

function createDockerfile()
{
    local projet="$1"
    local type="$2"
    local dest="$3"

    case "$type" in
        php)
        
            cat >"$BASE/$projet/start-apache" <<EOF
#!/usr/bin/env bash
sed -i "s/Listen 80/Listen \${PORT:-80}/g" /etc/apache2/ports.conf
sed -i "s/:80/:\${PORT:-80}/g" /etc/apache2/sites-enabled/*
apache2-foreground
EOF
            chmod 755 "$BASE/$projet/start-apache"
            cat >"$BASE/$projet/000-default.conf" <<EOF
# 000-default.conf
<VirtualHost *:80>
  ServerAdmin webmaster@localhost
  DocumentRoot /var/www/public

  <Directory /var/www>
    Options Indexes FollowSymLinks
    AllowOverride All
    Require all granted
  </Directory>
</VirtualHost>
EOF

            cat >"$TEMP/Dockerfile-$projet" <<EOF
FROM hub.eole.education/proxyhub/library/php:7.4-apache
COPY 000-default.conf /etc/apache2/sites-available/000-default.conf
COPY start-apache /usr/local/bin
RUN a2enmod rewrite
COPY . /var/www/
RUN chown -R www-data:www-data /var/www
EXPOSE 80
CMD ["start-apache"]
EOF
            ;;

        npm)
            cat >"$TEMP/Dockerfile-$projet" <<EOF
from hub.eole.education/proxyhub/library/node:latest as builder
RUN npm install -g npm@9.8.0
RUN npm install -g vite
RUN mkdir /src
ADD . /src
WORKDIR src
RUN npm install
RUN npm run build
#
FROM hub.eole.education/proxyhub/library/nginx:alpine
COPY --from=builder src/$dest /usr/share/nginx/html
VOLUME /usr/share/nginx/html
VOLUME /etc/nginx
EXPOSE 80
EOF
            ;;

        nuxt)
            cat >"$TEMP/Dockerfile-$projet" <<EOF
from hub.eole.education/proxyhub/library/node:latest as builder
RUN npm install -g npm@9.8.0
RUN npm install -g nuxt
RUN npm install -g @nuxtjs/eslint-module
RUN mkdir /src
ADD . /src
WORKDIR src
RUN npm install
RUN npm run build
#
FROM hub.eole.education/proxyhub/library/nginx:alpine
COPY --from=builder src/$dest /usr/share/nginx/html
VOLUME /usr/share/nginx/html
VOLUME /etc/nginx
EXPOSE 80
EOF
            ;;

        vite)
            cat >"$TEMP/Dockerfile-$projet" <<EOF
from hub.eole.education/proxyhub/library/node:latest as builder
RUN npm install -g npm@9.8.0
RUN npm install -g vite
RUN mkdir /src
ADD . /src
WORKDIR src
RUN npm install
RUN npm run build
#
FROM hub.eole.education/proxyhub/library/nginx:alpine
COPY --from=builder src/$dest /usr/share/nginx/html
VOLUME /usr/share/nginx/html
VOLUME /etc/nginx
EXPOSE 80
EOF
            ;;

        electron)
            cat >"$TEMP/Dockerfile-$projet" <<EOF
from hub.eole.education/proxyhub/library/node:latest as builder
RUN npm install -g npm@9.6.6
RUN npm install -g vite
RUN mkdir /src
ADD . /src
WORKDIR src
RUN npm install
WORKDIR src/electron
RUN npm install
RUN npm run build
#
FROM hub.eole.education/proxyhub/library/nginx:alpine
COPY --from=builder src/$dest /usr/share/nginx/html
VOLUME /usr/share/nginx/html
VOLUME /etc/nginx
EXPOSE 80
EOF
            ;;

        *)
            echo "$type non géré"
    esac    
}

function doSynchro()
{
    local projet="$1"
    local type="$2"
    local dest="$3"
 
    echo ""
    echo ""
    echo ""
    echo "===================================================================================="
    echo "$projet"
 
    cd "$BASE/$projet" || exit 1
    VERSION=""
    REV="$(git rev-list --tags --max-count=1)"
    VERSION=$(git describe --tags "$REV")
    if [ -z "$VERSION" ]
    then
        echo "Pas de version -> latest !!"
        VERSION=dev
        DATE_COMMIT=$(git log --format='%as' --no-walk )
        echo "DATE_COMMIT=$DATE_COMMIT"
    else
        DATE_COMMIT=$(git log --format='%as' --no-walk "$REV")
        echo "DATE_COMMIT=$DATE_COMMIT"
    fi
    echo "VERSION=$VERSION"
    
    echo "* check image $projet"
    BASE_IMAGE="hub.eole.education/test"
    IMAGE="$BASE_IMAGE/$projet:$VERSION"
    
    createDockerfile "$projet" "$type" "$dest"
    if [ -f "$TEMP/Dockerfile-$projet" ]
    then
        if ! diff "$BASE_BUILDER/Dockerfile-$projet" "$TEMP/Dockerfile-$projet" 
        then
            echo "* Dockerfile to be updated"
            #cp "$TEMP/Dockerfile-$projet" "$BASE_BUILDER/Dockerfile-$projet" 
        else
            echo "* Dockerfile up to date"
        fi
    fi
    
    echo "* Image $IMAGE"
    if docker manifest inspect "$IMAGE" >/dev/null
    then
        echo "image $IMAGE existe"
        return 0
    fi

    echo "* build docker $projet $2"
    docker build  --no-cache --progress=plain -f "$BASE_BUILDER/Dockerfile-$projet" -t "$IMAGE" .
    docker push "$IMAGE"
    docker tag "$IMAGE" "$BASE_IMAGE/$projet:latest"
}

function main()
{
    local filtre="$1"
    
    if [ ! -f ~/.gitlab/token ]
    then
        echo "Créer ~/.gitlab/token avec :"
        echo "HARBOR_TOKEN=<token>"
        echo "HARBOR_USERNAME=<username-harbor>"
        exit 1
    fi
    # shellcheck disable=SC1090
    source ~/.gitlab/token
    echo "$HARBOR_TOKEN" | docker login hub.eole.education -u "$HARBOR_USERNAME" --password-stdin

    BASE_BUILDER="$(pwd)"
    echo "BASE_BUILDER=$BASE_BUILDER"
    BASE="$HOME/ladigitale/"
    echo "BASE=$BASE"
    mkdir -p "$BASE"
    while IFS='|' read -r projet type dest
    do
        if [ -n "$filtre" ] && [ "$filtre" != "$projet" ]
        then
            continue
        fi
        doSynchro "$projet" "$type" "$dest"
    done <"$BASE_BUILDER/liste_projets.csv"
}

SCRIPT_PATH="${BASH_SOURCE[0]}"
if [ -h "${SCRIPT_PATH}" ]
then
  while [ -h "${SCRIPT_PATH}" ]
  do
      SCRIPT_PATH=$(readlink "${SCRIPT_PATH}")
  done
fi
pushd . > /dev/null
DIR_SCRIPT=$(dirname "${SCRIPT_PATH}" )
cd "${DIR_SCRIPT}" > /dev/null || exit 1
SCRIPT_PATH=$(pwd)
main "$@"
popd  > /dev/null || exit 1

