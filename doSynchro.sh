#!/bin/bash -xe

function doSynchro()
{
    local projet="$1"
    local type="npm"
    local dest="public"
 
    echo ""
    echo ""
    echo ""
    echo "===================================================================================="
    echo "$projet"
 
    echo "* updateDepot $1"
    if [ -d "$BASE/$projet" ]
    then
        cd "$BASE/$projet" || return 1
        git reset --hard
    else
        cd "$BASE" || return 1
        git clone "https://codeberg.org/ladigitale/$projet.git"
    fi
    git pull
    git config --global --add safe.directory "$BASE/$projet"
    
    REV="$(git rev-list --tags --max-count=1)"
    VERSION=$(git describe --tags "$REV")
    echo "VERSION=$VERSION"
    if [ -z "$VERSION" ]
    then
        echo "Pas de version -> latest !!"
        VERSION=latest
        DATE_COMMIT=$(git log --format='%as' --no-walk )
        echo "DATE_COMMIT=$DATE_COMMIT"
    else
        DATE_COMMIT=$(git log --format='%as' --no-walk "$REV")
        echo "DATE_COMMIT=$DATE_COMMIT"
    fi
    MASTER=$(git branch -r | grep /HEAD | sed 's#.*-> origin/##' )
    echo "MASTER=$MASTER"
    
    echo "* pushToMimLibre $1 $2"
    if grep -q mimlibre "$BASE/$projet/.git/config"
    then
        echo "synchro DONE"
        git push mimlibre refs/heads/* refs/tags/*
    else
        echo "synchro TODO"
        git remote add mimlibre "git@gitlab.mim-libre.fr:ladigitale/apps/${projet}.git"
        git push mimlibre "$MASTER"
    fi
}

function main()
{
    BASE_BUILDER="$(pwd)"
    echo "BASE_BUILDER=$BASE_BUILDER"
    BASE="$HOME/ladigitale/"
    echo "BASE=$BASE"
    mkdir -p "$BASE"
    while IFS='|' read -r projet type dest
    do
        doSynchro "$projet" "$type" "$dest"
    done <"$BASE_BUILDER/liste_projets.csv"
}

SCRIPT_PATH="${BASH_SOURCE[0]}"
if [ -h "${SCRIPT_PATH}" ]
then
  while [ -h "${SCRIPT_PATH}" ]
  do
      SCRIPT_PATH=$(readlink "${SCRIPT_PATH}")
  done
fi
pushd . > /dev/null
DIR_SCRIPT=$(dirname "${SCRIPT_PATH}" )
cd "${DIR_SCRIPT}" > /dev/null || exit 1
SCRIPT_PATH=$(pwd)
main
popd  > /dev/null || exit 1

